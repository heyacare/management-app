import HeyaCareClient from './request';

const BASEURL = 'notification';

const getNotifications = (data: any) => {
  return HeyaCareClient.post(BASEURL + '/', data);
};

export default {
  getNotifications,
};