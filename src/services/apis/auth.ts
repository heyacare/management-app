import { browserHistory } from 'helpers';
import HeyaCareClient from './request';
import { cookieConstants } from '../../constants';
import Jwt from 'jsonwebtoken';
import Cookies from 'js-cookie';

const baseUrl = 'users';

const isLoggedIn = () => {
  const accessToken = getAccessToken();
  return !!accessToken;
};

const login = (username: string, password: string) => {
  return HeyaCareClient.post(baseUrl + '/login', {
    username: username,
    password: password,
  });
};

const logout = () => {
  Cookies.remove(cookieConstants.HEYACARE_ACCESS_TOKEN);
  return HeyaCareClient.post(baseUrl + '/logout');
};

const setAccessToken = (accessToken: string) => {
  const decoded = Jwt.decode(accessToken);

  Cookies.set(cookieConstants.HEYACARE_ACCESS_TOKEN, accessToken, {
    expires: new Date((decoded as any).exp * 1000),
    secure: (window as any).BACKEND_URL.includes('https'),
  });
};

const getAccessToken = () => {
  return Cookies.get(cookieConstants.HEYACARE_ACCESS_TOKEN);
};

const getFullUserInfo = async () => {
  return HeyaCareClient.get(baseUrl + '/get_status');
};

const denyAccess = () => {
  browserHistory.push('/403');
};

export default {
  isLoggedIn,
  login,
  logout,
  getAccessToken,
  getFullUserInfo,
  denyAccess,
  setAccessToken,
};
