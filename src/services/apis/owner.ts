import HeyaCareClient from './request';

const BASE_URL = 'owner';

const createAccommodation = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/create', data);
};

const updateAccommodation = (data: any) => {
  return HeyaCareClient.put(BASE_URL + '/accommodation/update', data);
};

const getAccommodation = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/get', data);
};

const changeRentalStatus = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/change-rental-status', data);
};

const getViewAnalytic = (accommodationId: string) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/view/analysis', { accommodation_id: accommodationId });
};

const register = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/register', data);
};

const confirmEmail = (token: string) => {
  const url = '/register/owner/confirm_email/' + token;
  return HeyaCareClient.get(url);
};

const extendDisplayTime = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/extend', data);
};

export default {
  createAccommodation,
  getAccommodation,
  changeRentalStatus,
  getViewAnalytic,
  register,
  confirmEmail,
  updateAccommodation,
  extendDisplayTime,
};