import HeyaCareClient from './request';

const BASE_URL = 'file';

const uploadImage = (formData: any) => {
  return HeyaCareClient.post(BASE_URL + '/store-image', formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

export default {
  uploadImage,
};