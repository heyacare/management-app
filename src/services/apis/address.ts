import HeyaCareClient from './request';

const BASE_URL = 'address';

const getAllCity = () => {
  return HeyaCareClient.get(BASE_URL + '/city');
};

const getDistrictByCityCode = (city_code: string) => {
  return HeyaCareClient.get(BASE_URL + `/district/${city_code}`);
};

const getWardByDistrictCode = (district_code: string) => {
  return HeyaCareClient.get(BASE_URL + `/ward/${district_code}`);
};

export default {
  getAllCity,
  getDistrictByCityCode,
  getWardByDistrictCode,
};