import HeyaCareClient from './request';

const BASE_URL = 'accommodation';

const getTypes = () => {
  return HeyaCareClient.get(BASE_URL + '/types');
};

const getRatingAndComments = (accommodationId: string) => {
  return HeyaCareClient.post(BASE_URL + '/rating/get', {
    star: 0,
    accommodation_id: accommodationId,
    page: 1,
    limit: 1000,
  });
};

const getRoomById = (roomId: string) => {
  return HeyaCareClient.get(BASE_URL + `/owner/${roomId}`);
};

const getRoomByIdForUpdate = (roomId: string) => {
  return HeyaCareClient.get(BASE_URL + `/owner/${roomId}/for-update`);
};

export default {
  getTypes,
  getRatingAndComments,
  getRoomById,
  getRoomByIdForUpdate,
};