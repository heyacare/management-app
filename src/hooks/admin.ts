import { useEffect, useState } from 'react';
import { adminServices } from '../services';

export interface IOwnerRequest {
  id: number;
  fullname: string;
  email: string;
  phone_number: string;
  address: string;
  created_at: string;
  identity_card_number: string;
  avatar_url: string;
}

const useOwnerRequest = () => {
  const [owners, setOwners] = useState<IOwnerRequest[]>([]);
  const [totalOwners, setTotalOwners] = useState<number>(0);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    adminServices.getOwnerRequest({ _page: page, _limit: limit }).then(res => {
      setOwners(res.data.owners);
      setTotalOwners(res.data.total);
      setReload(false);
    });
  }, [reload]);

  const confirmApprovalOwner = (ownerId: number) => {
    return adminServices.confirmApprovalOwner(ownerId);
  };

  return {
    owners,
    page,
    limit,
    totalOwners,
    setReload,
    reload,
    setPage,
    setLimit,
    confirmApprovalOwner,
  };
};

interface IOwner {
  id: number;
  fullname: string;
}

export interface IAccommodationRequest {
  id: string;
  title: string;
  created_at: string;
  owner: IOwner;
  images: string[];
}

const useAccommodationRequest = () => {
  const [accommodations, setAccommodations] = useState<IAccommodationRequest[]>([]);
  const [total, setTotal] = useState<number>(0);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    adminServices.getAccommodationRequest({ _page: page, _limit: limit }).then(res => {
      setAccommodations(res.data.accommodations);
      setTotal(res.data.total);
      setReload(false);
    });
  }, [reload]);

  const confirmApprovalAccommodation = (accommodationId: string) => {
    return adminServices.confirmApprovalAccommodation(accommodationId);
  };

  const declineAccommodation = (accommodationId: string) => {
    return adminServices.declineAccommodation(accommodationId);
  };

  return {
    accommodations,
    page,
    limit,
    total,
    setReload,
    reload,
    setPage,
    setLimit,
    confirmApprovalAccommodation,
    declineAccommodation,
  };
};

export default {
  useOwnerRequest,
  useAccommodationRequest,
};