import { useEffect, useState } from 'react';
import { accommodationServices, ownerServices } from '../services';
import { IAccommodationOwnerRes } from '../interfaces/accommodation';

interface AccommodationType {
  id: number;
  name: string;
}

const useTypes = () => {
  const [accommodationTypes, setAccommodationTypes] = useState<AccommodationType[]>([]);

  useEffect(() => {
    accommodationServices.getTypes().then(res => {
      setAccommodationTypes(res.data.data);
    });
  }, []);
  return {
    accommodationTypes,
  };
};

const useOwnerAccommodation = (refresh: boolean) => {
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [isLoading, setIsLoading] = useState(true);
  const [accommodationRes, setAccommodationRes] = useState<IAccommodationOwnerRes>({
    total: 0,
    accommodations: [],
  });

  useEffect(() => {
    setIsLoading(true);
    ownerServices.getAccommodation({ _page: page, _limit: limit }).then((res) => {
      setAccommodationRes(res.data.data);
      setIsLoading(false);
    });
  }, [refresh, page, limit]);

  return {
    page, setPage,
    limit, setLimit,
    accommodationRes,
    isLoading,
  };
};

interface IRoom {
  'id': string,
  'title': string,
  'description': string,
  'area': number,
  'electricity_price': number,
  'water_price': number,
  'city_code': string,
  'district_code': string,
  'ward_code': string,
  'accommodation_type_id': number,
  'bathroom_type': number,
  'kitchen_type': number,
  'number_of_room': number,
  'street_address': number,
  'number_of_days_display': number,
  'images': string[],
  'nearby_locations': string[],
  'is_stay_with_the_owner': boolean,
  'has_electric_water_heater': boolean,
  'has_air_conditioning': boolean,
  'has_balcony': boolean,
  'price': number
}

const useRoomEdit = (roomId: string) => {
  const [room, setRoom] = useState<IRoom>();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    accommodationServices.getRoomByIdForUpdate(roomId).then((res: any) => {
      setRoom(res.data);
      setIsLoading(false);
    });
  }, [roomId]);

  return {
    room,
    isLoading,
  };
};

export default {
  useTypes,
  useOwnerAccommodation,
  useRoomEdit,
};