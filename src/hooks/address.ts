import { useEffect, useState } from 'react';
import { ICity, IDistrict, IWard } from '../interfaces';
import { addressServices } from '../services';

const useAddressHook = () => {
  const [cities, setCities] = useState<ICity[]>([]);
  const [districts, setDistricts] = useState<IDistrict[]>([]);
  const [wards, setWards] = useState<IWard[]>([]);
  const [cityCode, setCityCode] = useState<string>('');
  const [districtCode, setDistrictCode] = useState<string>('');

  useEffect(() => {
    addressServices.getAllCity().then(res => {
      setCities(res.data);
    });
  }, []);

  useEffect(() => {
    if (cityCode) {
      addressServices.getDistrictByCityCode(cityCode).then(res => {
        setDistricts(res.data);
      });
    }
  }, [cityCode]);

  useEffect(() => {
    if (districtCode) {
      addressServices.getWardByDistrictCode(districtCode).then(res => {
        setWards(res.data);
      });
    }
  }, [districtCode]);

  return {
    cities,
    districts,
    wards,
    setCityCode,
    setDistrictCode,
  };
};

export default {
  useAddressHook,
};