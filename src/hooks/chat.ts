import { useEffect, useState } from 'react';
import { chatServices } from '../services';


interface Message {
  content: string;
  type_of_message: string;
  sender_id: number;
  receiver_id: number;
}


const useMessage = (receiverId: number) => {
  const [messages, setMessages] = useState<Message[]>([]);

  useEffect(() => {
    chatServices.getMessages(receiverId).then(res => setMessages(res.data));
  }, [receiverId]);
  return {
    messages,
    setMessages,
  };
};

export default {
  useMessage,
};