import React, { useContext } from 'react';
import { Typography } from 'antd';
import classNames from 'classnames';
import { StoreContext } from '../../../contexts';
import Chat from '../../Chat';

const { Title } = Typography;

interface AppContainerProps {
  title: React.ReactNode;
  head?: React.ReactNode;
  className?: string;
}

const AppContainer: React.FC<AppContainerProps> = props => {
  const { title, head, className, children } = props;

  const { currentUser } = useContext(StoreContext);

  return (
    <div className="app-container">
      <div className="app-container-head">
        <Title className="app-title" level={4}>
          {title}
        </Title>
        {head}
      </div>

      {children && (
        <div
          className={classNames({
            'app-container-body': true,
            ...(className && { [className]: true }),
          })}
        >
          {children}
          {currentUser.role == 'owner' && <Chat />}
        </div>
      )}
    </div>
  );
};

export default AppContainer;
