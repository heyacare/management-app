import React from 'react';
import AppContainer from '../AppLayout/AppContainer';
import ContentBlock from '../../components/shared/ContentBlock';
import { Typography } from 'antd';
import UserAnalytics from '../../components/Analytics/UserAnalytics';
import ViewAnalytics from '../../components/Analytics/ViewAnalytics';

const Analytics = () => {
  return (
    <AppContainer title={'Thống kê'}>
      <ContentBlock className={'text-center'}>
        <Typography.Title className={'text-center'} level={4}>Thống kê dữ liệu</Typography.Title>
        <UserAnalytics />
        <ViewAnalytics />
      </ContentBlock>
    </AppContainer>
  );
};

export default Analytics;