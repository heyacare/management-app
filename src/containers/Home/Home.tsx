import React, { useContext } from 'react';
import { t } from 'helpers/i18n';
import AppContainer from 'containers/AppLayout/AppContainer';
import ContentBlock from '../../components/shared/ContentBlock';
import { StoreContext } from '../../contexts';
import { Typography } from 'antd';
import advantages from '../../assets/images/avantages.png';

const Home: React.FC = () => {
  const { currentUser } = useContext(StoreContext);

  return (
    <AppContainer title={t('Trang chủ')}>
      <ContentBlock className={'text-center'} style={{ height: "100%"}}>
        {currentUser.role === 'renter' &&
        <Typography.Title level={5}
        >Tài khoản của bạn đang trong quá trình phê duyệt. Hãy quay lại sau khi được xác nhận
          nhé ! </Typography.Title>
        }
        <Typography.Title level={3}
                          style={{color: '#30456E'}}
        >HEYACARE TIẾP CẬN KHÁCH HÀNG NHANH CHÓNG - HIỆU QUẢ</Typography.Title>
        <img src={advantages} alt={''} />
      </ContentBlock>
    </AppContainer>
  );
};

export default Home;
