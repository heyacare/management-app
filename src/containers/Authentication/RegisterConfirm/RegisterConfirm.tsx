import React, { useEffect, useState } from 'react';
import logo from '../../../assets/images/logo.png';
import { Spin, Typography } from 'antd';
import { Link } from 'react-router-dom';
import { ArrowRightOutlined } from '@ant-design/icons';
import { ownerServices } from '../../../services';

const RegisterConfirm = (props: any) => {
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    ownerServices.confirmEmail(props.match.params.token).then(res => {
      setIsSuccess(true);
      setIsLoading(false);
    }).catch(() => {
      setIsLoading(false);
    });
  }, []);

  return (
    <div className={'login'}>
      <div className="login__body">
        <img src={logo} alt={'heyacare_logo'} />
        <h3> Quản lí và cho thuê phòng trọ thật dễ dàng 😍</h3>

        {
          isSuccess && (
            <>
              <Typography.Title level={4}>
                Xác thực thành công!
              </Typography.Title>
              <Link to={'/login'}>Quay lại trang đăng nhập <ArrowRightOutlined /></Link>
            </>
          )
        }
        {
          isLoading && (
            <>
              <Spin className={'app-spinner'}></Spin>
              <Typography.Title level={4}>
                Đang xác thực
              </Typography.Title>
            </>
          )
        }
        {
          !isLoading && !isSuccess ? (
            <Typography.Title level={4}>
              Xác thực thất bại
            </Typography.Title>
          ) : null
        }
      </div>
    </div>
  );
};

export default RegisterConfirm;