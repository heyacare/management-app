import React, { useEffect, useState } from 'react';
import AppContainer from '../../AppLayout/AppContainer';
import ContentBlock from '../../../components/shared/ContentBlock';
import { Button, Col, notification, Row, Typography } from 'antd';
import {
  IRatingAndComment,
  RatingAndCommentItem,
} from '../../../components/Acommodation/AccommodationTable/RatingAndComment/RatingAndComment';
import { adminServices } from '../../../services';

import { CheckOutlined, CloseOutlined } from '@ant-design/icons';

const ApprovalRating = () => {
  const [ratings, setRatings] = useState<IRatingAndComment[]>([]);

  useEffect(() => {
    adminServices.getRatingRequest({
      _page: 1,
      _limit: 100,
    }).then((res: any) => setRatings(res.data.data.ratings));
  }, []);

  const hideRating = (index: number) => {
    setRatings(ratings => {
      return [
        ...ratings.slice(0, index),
        ...ratings.slice(index + 1),
      ];
    });
  };

  return (
    <AppContainer title={'Phê duyệt đánh giá'}>
      <ContentBlock>
        <Typography.Title className={'text-center'} level={4}> Phê duyệt đánh giá </Typography.Title>
        {
          ratings.map((item, index, array) => (
            <Row>
              <Col span={8} offset={6}>
                <RatingAndCommentItem item={item} />
              </Col>
              <Col span={6} className={'d-flex align-items-center'}>
                <Button type={'link'}
                        icon={<CheckOutlined />}
                        onClick={
                          () => {
                            adminServices.confirmApprovalRating(item.id).then(() => {
                              notification.success({
                                message: 'Phê duyệt đánh giá thành công !',
                                duration: 2,
                              });
                              hideRating(index);
                            });
                          }
                        }
                >
                  Phê duyệt
                </Button>
                <Button type={'link'}
                        icon={<CloseOutlined />}
                        onClick={() => {
                          adminServices.declineRating(item.id).then(() => {
                            notification.success({
                              message: 'Từ chối đánh giá thành công !',
                              duration: 2,
                            });
                            hideRating(index);
                          });
                        }}
                >
                  Từ chối
                </Button>
              </Col>
            </Row>
          ))
        }
      </ContentBlock>
    </AppContainer>
  );
};

export default ApprovalRating;