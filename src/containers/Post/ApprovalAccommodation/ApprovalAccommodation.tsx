import React from 'react';
import AppContainer from '../../AppLayout/AppContainer';
import ContentBlock from '../../../components/shared/ContentBlock';
import ApprovalAccommodTable from '../../../components/Acommodation/ApprovalAccommodTable';
import { Typography } from 'antd';

const ApprovalAccommodation = () => {
  return (
    <AppContainer title={'Phê duyệt bài viết'}>
      <ContentBlock>
        <Typography.Title level={4} className={'text-center'}> Phê duyệt bài viết </Typography.Title>
        <ApprovalAccommodTable />
      </ContentBlock>
    </AppContainer>
  );
};

export default ApprovalAccommodation;