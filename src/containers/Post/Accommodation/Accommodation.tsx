import React, { useState } from 'react';
import AppContainer from '../../AppLayout/AppContainer';
import ContentBlock from '../../../components/shared/ContentBlock';
import AccommodationTable from '../../../components/Acommodation/AccommodationTable';
import AccommodationCreate from '../../../components/Acommodation/AccommodationCreate';
import { Button, Typography } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import './Accommodation.scss';

const Accommodation = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [refresh, setRefresh] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const extra = () => {
    return (
      <Button
        type={'primary'}
        icon={<EditOutlined />}
        onClick={showModal}
      >
        Tạo bài đăng mới
      </Button>
    );
  };

  return (
    <AppContainer title={'Bài đăng'}>
      <ContentBlock extra={extra()}>
        <AccommodationCreate
          isVisible={isModalVisible}
          handleOk={handleOk}
          handleCancel={handleCancel}
          handleRefresh={setRefresh}
        />
        <Typography.Title level={2} className={'text-center'}> Quản lý bài đăng </Typography.Title>
        <AccommodationTable refresh={refresh} />
      </ContentBlock>
    </AppContainer>
  );
};

export default Accommodation;