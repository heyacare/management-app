import AppContainer from '../../AppLayout/AppContainer';
import ContentBlock from '../../../components/shared/ContentBlock';
import React, { useContext, useEffect, useState } from 'react';
import { message, Tag, Typography } from 'antd';
import { StoreContext } from '../../../contexts';
import OwnerRequestTable from '../../../components/User/OwnerRequestTable';

const ApprovalOwner = () => {
  return (
    <AppContainer title={'Phê duyệt chủ trọ'}>
      <ContentBlock>
        <Typography.Title className={'text-center'} level={4}>Phê duyệt chủ trọ</Typography.Title>
        <OwnerRequestTable />
      </ContentBlock>
    </AppContainer>
  )
};

export default ApprovalOwner;