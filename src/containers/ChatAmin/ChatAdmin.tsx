import AppContainer from '../AppLayout/AppContainer';
import ContentBlock from '../../components/shared/ContentBlock';
import React, { useContext } from 'react';
import ChatAdmin from '../../components/ChatAmin/ChatAdmin';
import { StoreContext } from '../../contexts';

const ChatAmin = (props: any) => {
  const { match: { params } } = props;
  const { currentUser } = useContext(StoreContext);
  console.log(params);

  return (
    <AppContainer title={'Nhắn tin với chủ trọ'}>
      <ContentBlock>
        <ChatAdmin senderId={currentUser.id} receiverId={parseInt(params.user_id)} />
      </ContentBlock>
    </AppContainer>
  );
};

export default ChatAmin;