/*
 * You can mock AntD components in this file
 */
import React from 'react';
import { DropDownProps } from 'antd/lib/dropdown';

interface MockType {
  dropdown: DropDownProps & { children: React.ReactNode };
}

// Dropdown
jest.mock('antd/lib/dropdown', () => {
  const Dropdown = (props: MockType['dropdown']) => {
    const { overlay, children } = props;

    return (
      <div className="dropdown">
        <div className="dropdown-overlay">{overlay}</div>
        <div className="dropdown-content">{children}</div>
      </div>
    );
  };
  return Dropdown;
});
