import { createContext } from 'react';
import { currentUser } from 'services/mocks/user';
import { IUserDetailsInfo } from '../interfaces';

interface StoreContextType {
  currentUser: IUserDetailsInfo;
}

export const StoreContext = createContext<StoreContextType>({
  currentUser,
});
