import React from 'react';
import { Button, Col, Row, Statistic } from 'antd';

const UserAnalytics = () => {
  return (
    <Row gutter={16} justify={'space-around'}>
      <Col span={8}>
        <Row>
          <Col span={12}>
            <Statistic title="Người dùng hoạt động" value={10000} />
          </Col>
          <Col span={12}>
            <Statistic title="Chủ trọ" value={125} precision={2} />
          </Col>
        </Row>
      </Col>
      <Col span={8}>
        <Row>
          <Col span={8}>
            <Statistic title="Số bài đăng" value={10000} />
          </Col>
          <Col span={8}>
            <Statistic title="Lượt xem" value={125} precision={2} />
          </Col>
          <Col span={8}>
            <Statistic title="Lượt đánh giá trung bình" value={3.6} precision={2} />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default UserAnalytics;