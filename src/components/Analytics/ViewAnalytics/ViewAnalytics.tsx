import React, { useState } from 'react';
import { Line } from 'react-chartjs-2';
import { Col, Row } from 'antd';

const ViewAnalytics = () => {
  const [views, setViews] = useState<IView[]>([]);

  const labels = views.map(item => item.time);
  const data = views.map(item => item.value);

  return (
    <Row className={'text-center'}>
      <Col span={12}>
        <Line
          data={{
            labels: labels,
            datasets: [
              {
                data: data,
                label: 'Xem trang',
                borderColor: '#3e95cd',
                fill: false,
              },
            ],
          }}
          options={{
            title: {
              display: true,
              text: 'Lưu lượng truy cập',
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          }}
        />
      </Col>
      <Col span={12}>
        <Line
          data={{
            labels: labels,
            datasets: [
              {
                data: data,
                label: 'Tìm kiếm',
                borderColor: '#3e95cd',
                fill: false,
              },
            ],
          }}
          options={{
            title: {
              display: true,
              text: 'Lưu lượng tìm kiếm',
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          }}
        />
      </Col>
    </Row>
  );
};

interface IView {
  time: string;
  value: number
}

export default ViewAnalytics;