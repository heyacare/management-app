import React, { useEffect, useState } from 'react';
import { IRoomPreview } from '../../../interfaces/accommodation';
import { accommodationServices } from '../../../services';
import { Carousel, Col, Row, Spin, Typography } from 'antd';
import { HomeOutlined } from '@ant-design/icons';

const contentStyle = {
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const RoomPreview = (props: IProps) => {
  const [room, setRoom] = useState<IRoomPreview>();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    accommodationServices.getRoomById(props.roomId).then(
      (res: any) => {
        setRoom(res.data.data);
        setIsLoading(false);
      },
    );
  }, [props.roomId]);

  if (isLoading) {
    return <Spin className={'app-spinner text-center'}></Spin>;
  }

  if (room) {
    return (
      <Row>
        <Col
          // span={18} offset={3}
        >
          <Carousel className={'text-center'} autoplay>
            {room.images.map(image => (
              <div>
                <img
                  src={image}
                  style={{
                    width: '100%',
                  }}
                />
              </div>
            ))}
          </Carousel>,
          <Typography.Title level={4}>
            {room.title}
          </Typography.Title>
          <Typography.Text>
            <HomeOutlined /> : {room.street_address + ', ' + room.address}
          </Typography.Text>

          <Row className={'mt-base mb-base'}>
            <Col span={12}>
              <Typography.Title level={4}>
                Thông tin chính
              </Typography.Title>
              <Typography.Paragraph>
                <p>
                  - Loại phòng : {room.accommodation_type}
                </p>
                <p>
                  - Diện tích : {room.area} <span>m<sup>2</sup></span>
                </p>
                <p>
                  - Giá thuê : {numberWithCommas(room.price)} đồng
                </p>
                <p>
                  - Giá điện : {numberWithCommas(room.electricity_price)} đồng/số
                </p>
                <p>
                  - Giá nước : {numberWithCommas(room.water_price)} đồng/khối
                </p>
                <p>
                  - {room.is_stay_with_the_owner ? 'Chung chủ' : 'Không chung chủ'}
                </p>
              </Typography.Paragraph>

            </Col>
            <Col span={12}>
              <Typography.Title level={4}>
                Các tiện tích
              </Typography.Title>
              <Typography.Paragraph>
                <p>
                  - Nhà tắm {room.bathroom_type}
                </p>
                <p>
                  - {room.kitchen_type}
                </p>
                <p>
                  - {room.has_air_conditioning ? 'Có điều hòa' : 'Không có điều hòa'}
                </p>
                <p>
                  - {room.has_electric_water_heater ? 'Có bình nóng lạnh' : 'Không có bình nóng lạnh'}
                </p>
                <p>
                  - {room.has_balcony ? 'Có ban công' : 'Không có ban công'}
                </p>
              </Typography.Paragraph>
            </Col>
          </Row>

          <Typography.Title level={4}>
            Mô tả thêm
          </Typography.Title>
          <Typography.Paragraph>
            {room.description}
          </Typography.Paragraph>

          <Typography.Title level={4}>
            Địa chỉ lân cận
          </Typography.Title>
          <Typography.Paragraph>
            {room.nearby_locations.map(location => <p> - {location} </p>)}
          </Typography.Paragraph>
          <Typography.Title level={4}>
            Liên hệ
          </Typography.Title>
          <Typography.Paragraph>
            - Chủ trọ : {room.owner.fullname} <br/>
            - Số điện thoại : {room.owner.phone_number} <br/>
            - Email : {room.owner.email} <br/>
          </Typography.Paragraph>
        </Col>
      </Row>
    );
  }
  return null;
};

interface IProps {
  roomId: string
}

export default RoomPreview;