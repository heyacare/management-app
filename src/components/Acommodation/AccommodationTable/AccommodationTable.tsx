import React, { useState } from 'react';
import { Button, Image, Modal, Pagination, Popover, Table, Tag, Radio, notification, Input, InputNumber } from 'antd';
import './AccommodationTable.scss';
import { accommodationHooks } from '../../../hooks';
import { IAccommodationOwner } from '../../../interfaces/accommodation';
import {
  EditOutlined,
  EyeOutlined,
  LineChartOutlined,
  PlusCircleOutlined,
  CheckOutlined,
  CloseOutlined,
} from '@ant-design/icons';
import ViewAnalytic from './ViewAnalytic';
import RatingAndComment from './RatingAndComment';
import moment from 'moment';
import RoomPreview from '../RoomPreview';
import RoomEdit from '../RoomEdit/RoomEdit';
import { ownerServices } from '../../../services';

interface Iprops {
  refresh: boolean
}

interface IRecord {
  accommodation: IAccommodationOwner;
  editId: string;
  setEditId: (accommodationId: string) => void;
  setIsViewModalVisible: (visible: boolean) => void;
  setIsRatingModalVisible: (visible: boolean) => void;
  setIsPreviewModalVisible: (visible: boolean) => void;
  setIsEditModalVisible: (visible: boolean) => void;
  setIsChangeIsRentedModalVisible: any;
  isEditIsRented: boolean;
  setIsEditIsRented: any;
  isExtendDisplayTime: boolean;
  setIsExtendDisplayTime: any;
}

const AccommodationTable = (props: Iprops) => {

  const { refresh } = props;

  const [editId, setEditId] = useState('');
  const [isViewModalVisible, setIsViewModalVisible] = useState(false);
  const [isRatingModalVisible, setIsRatingModalVisible] = useState(false);
  const [isPreviewModalVisible, setIsPreviewModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [isEditIsRented, setIsEditIsRented] = useState(false);
  const [isExtendDisplayTime, setIsExtendDisplayTime] = useState(false);

  const { accommodationRes, setPage, setLimit, isLoading } = accommodationHooks.useOwnerAccommodation(refresh);

  const data = accommodationRes?.accommodations.map(item => ({
      accommodation: item,
      setEditId: setEditId,
      setIsViewModalVisible: setIsViewModalVisible,
      setIsRatingModalVisible: setIsRatingModalVisible,
      setIsPreviewModalVisible: setIsPreviewModalVisible,
      setIsEditModalVisible: setIsEditModalVisible,
      editId: editId,
      isEditIsRented: isEditIsRented,
      setIsEditIsRented: setIsEditIsRented,
      isExtendDisplayTime: isExtendDisplayTime,
      setIsExtendDisplayTime: setIsExtendDisplayTime,
    } as IRecord),
  );

  const onShowSizeChange = (current: number, pageSize: number) => {
    setLimit(pageSize);
  };

  const onChangePage = (page: number) => {
    if (page > 0) {
      setPage(page);
    }
  };

  const handleChange = (pagination: any, filters: any, sorter: any) => {
    setPage(1);
  };


  return (
    <div className={'text-center'}>
      <Table
        // @ts-ignore
        columns={columns}
        dataSource={data}
        bordered
        pagination={false}
        loading={isLoading}
      />
      <Pagination
        className={'m-base'}
        total={accommodationRes.total}
        showSizeChanger
        onShowSizeChange={onShowSizeChange}
        onChange={onChangePage}
      />

      <Modal
        visible={isViewModalVisible}
        onCancel={() => {
          setIsViewModalVisible(false);
          setEditId('');
        }}
        footer={null}
        width={1000}
      >
        <ViewAnalytic accommodationId={editId} />
      </Modal>
      <Modal
        visible={isRatingModalVisible}
        onCancel={() => {
          setIsRatingModalVisible(false);
          setEditId('');
        }}
        footer={null}
        width={500}
      >
        <RatingAndComment accommodationId={editId} />
      </Modal>
      <Modal
        title={'Xem thông tin bài đăng'}
        visible={isPreviewModalVisible}
        onCancel={() => {
          setIsPreviewModalVisible(false);
          setEditId('');
        }}
        footer={null}
        width={600}
      >
        <RoomPreview roomId={editId} />
      </Modal>
      <Modal
        title={'Chỉnh sửa bài đăng'}
        visible={isEditModalVisible}
        onCancel={() => {
          setIsEditModalVisible(false);
          setEditId('');
        }}
        footer={null}
        width={1000}
      >
        <RoomEdit roomId={editId} />
      </Modal>
    </div>
  );
};

const columns = [
  {
    title: 'Ảnh',
    key: 'image',
    render: (record: IRecord) => {
      return <Image className={'hover-pointer'} width={100} src={record.accommodation.images[0]} />;
    },
    align: 'center',
  },
  {
    title: 'Tiêu đề',
    key: 'title',
    render: (record: IRecord) => {
      return (
        <p>
          {record.accommodation.title}
          <p style={{ fontSize: '0.7rem', color: 'gray' }}> {record.accommodation.full_address} </p>
        </p>
      );
    },
  },
  {
    title: 'Hành động với bài viết',
    render: (record: IRecord) => {
      return (
        <>
          <Popover content={'Xem bài viết'}>
            <Button
              type="link"
              icon={<EyeOutlined />}
              onClick={() => {
                record.setIsPreviewModalVisible(true);
                record.setEditId(record.accommodation.id);
              }}
            > Xem </Button>
          </Popover>
          {record.accommodation.status === 'Đang chờ' &&
          <Popover content={'Chỉnh sửa bài viết'}>
            <Button
              type="link"
              icon={<EditOutlined />}
              onClick={() => {
                record.setIsEditModalVisible(true);
                record.setEditId(record.accommodation.id);
              }}
            > Sửa </Button>
          </Popover>
          }

        </>
      );
    },
    align: 'center',
  },
  {
    title: 'Trạng thái phòng',
    key: 'is_rented',
    render: (record: IRecord) => {
      if (record.editId === record.accommodation.id && record.isEditIsRented) {
        let isRented = record.accommodation.is_rented;
        return (
          <div className={'text-left'}>
            Trạng thái phòng : <br />
            <Radio.Group onChange={(e) => {
              isRented = e.target.value;
            }} defaultValue={isRented}>
              <Radio value={true}>Đã cho thuê</Radio>
              <Radio value={false}>Chưa cho thuê</Radio>
            </Radio.Group>
            <Button
              icon={<CheckOutlined />}
              type={'link'}
              onClick={() => {
                ownerServices.changeRentalStatus({
                  is_rented: isRented,
                  accommodation_id: record.accommodation.id,
                }).then(() => {
                  notification.success({
                    message: 'Cập nhật trạng thái phòng trọ thành công.',
                    duration: 2,
                  });
                  record.accommodation.is_rented = isRented;
                  record.setEditId('');
                  record.setIsEditIsRented(false);
                });
              }}
            >Lưu</Button>
          </div>
        );
      }

      return (
        <Popover content={'Thay đổi trạng thái'}
        >
          <div style={{ cursor: 'pointer' }}
               onClick={() => {
                 record.setEditId(record.accommodation.id);
                 record.setIsEditIsRented(true);
               }}>
            <Tag
              className={'hover-pointer tag'}
              color={getColorIsRented(record.accommodation.is_rented)}>{record.accommodation.is_rented ? 'Đã thuê' : 'Chưa thuê'}</Tag>

            <EditOutlined />

          </div>
        </Popover>
      );
    },
    align: 'center',
    filters: [
      {
        text: 'Chưa thuê',
        value: false,
      },
      {
        text: 'Đã thuê',
        value: true,
      },
    ],
    filterMultiple: false,
  },
  {
    title: 'Trạng thái phê duyệt',
    key: 'status',
    render: (record: IRecord) => {
      return <Tag className={'tag'}
                  color={colorStatus[record.accommodation.status]}> {record.accommodation.status} </Tag>;
    },
    align: 'center',
    filters: [
      {
        text: 'Đã phê duyệt',
        value: 1,
      },
      {
        text: 'Bị từ chối',
        value: -1,
      },
      {
        text: 'Đang chờ',
        value: 0,
      },
    ],
    filterMultiple: false,
  },
  {
    title: 'Số lượt xem ',
    key: 'number_of_views',
    render: (record: IRecord) => {
      return (
        <Popover content={'Thống kê lượt xem'}>
          <Button
            type={'link'}
            icon={<LineChartOutlined />}
            onClick={() => {
              record.setEditId(record.accommodation.id);
              record.setIsViewModalVisible(true);
            }}
          > {record.accommodation.number_of_views}
          </Button>
        </Popover>
      );
    },
    align: 'center',
  },
  {
    'title': 'Thời gian hiển thị còn lại',
    'key': 'time_remaining',
    render: (record: IRecord) => {
      let remaining = moment(record.accommodation.display_time).diff(moment(), 'days');
      if (record.editId === record.accommodation.id && record.isExtendDisplayTime) {
        let numberOfDays: any = 0;
        return (
          <div>
            <div>
              Gia hạn thêm : <InputNumber min={7} max={60} onChange={value => {
              numberOfDays = value;
            }} /> ngày
            </div>
            <Button icon={<CheckOutlined />} type={'link'}
                    onClick={() => {
                      ownerServices.extendDisplayTime({
                        accommodation_id: record.accommodation.id,
                        number_of_days: numberOfDays,
                      }).then(() => {
                        record.accommodation.display_time = '';
                        record.setIsExtendDisplayTime(false);
                        record.setEditId('');
                        record.accommodation.status = 'Đang chờ';
                        notification.success({
                          message: 'Gia hạn thêm thời gian hiển thị thành công. Bài viết đang chờ phê duyệt.',
                          duration: 2,
                        });
                        remaining = numberOfDays;
                      });
                    }}
            >Lưu</Button>
            <Button icon={<CloseOutlined />} type={'link'}
                    onClick={() => {
                      record.setEditId('');
                      record.setIsExtendDisplayTime(false);
                    }}
            >Quay lại</Button>
          </div>
        );
      }

      if (remaining > 0) {
        return (
          <span className={'mr-half'}>{remaining + ' ngày'}</span>
        );
      }
      if (remaining <= 0) {
        return (
          <Popover content={'Gia hạn thêm'} className={'hover-pointer'}>
            <div onClick={() => {
              record.setEditId(record.accommodation.id);
              record.setIsExtendDisplayTime(true);
            }}>
              <Tag className={'tag'} color={'red'}><span className={'mr-half'}>Hết hạn</span></Tag>
              <PlusCircleOutlined />
            </div>
          </Popover>
        );
      }
      return <span>_</span>;

    },
    align: 'center',
  }
  , {
    title: 'Đánh giá trung bình ',
    key: 'average_rating',
    render: (record: IRecord) => {
      return (
        <Popover content={'Xem bình luận và đánh giá'}>
          <Button
            type={'link'} icon={<EyeOutlined />}
            onClick={() => {
              record.setIsRatingModalVisible(true);
              record.setEditId(record.accommodation.id);
            }}
          > {record.accommodation.average_rating} </Button>
        </Popover>
      );
    },
    align: 'center',
  },

];

interface Color {
  [key: string]: string
}


const colorStatus: Color = {
  'Đã phê duyệt': 'green',
  'Bị từ chối': 'red',
  'Đang chờ': 'gold',
};

const getColorIsRented = (isRented: boolean) => {
  if (isRented) {
    return 'green';
  }
  return 'gold';
};

export default AccommodationTable;