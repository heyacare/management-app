import React, { useEffect, useState } from 'react';
import { ownerServices } from '../../../../services';
import { Line } from 'react-chartjs-2';

const ViewAnalytic = (props: IProps) => {
  const [views, setViews] = useState<IView[]>([]);

  useEffect(() => {
    if (props.accommodationId.length > 0) {
      ownerServices.getViewAnalytic(props.accommodationId).then(res => {
        setViews(res.data.data);
      });
    }
  }, [props.accommodationId]);

  const labels = views.map(item => item.time);
  const data = views.map(item => item.value);

  return (
    <Line
      data={{
        labels: labels,
        datasets: [
          {
            data: data,
            label: 'Lượt xem bài viết',
            borderColor: '#3e95cd',
            fill: false,
          },
        ],
      }}
      options={{
        title: {
          display: true,
          text: 'Lưu lượng truy cập',
        },
        legend: {
          display: true,
          position: 'bottom',
        },
      }}
    />
  );
};

interface IProps {
  accommodationId: string
}

interface IView {
  time: string;
  value: number
}

export default ViewAnalytic;