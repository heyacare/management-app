import React, { useContext, useEffect, useState } from 'react';
import { accommodationHooks, addressHooks, fileHooks } from '../../../hooks';
import { Button, Form, Input, InputNumber, notification, Radio, Select, Spin } from 'antd';
import { accommodationConstants } from '../../../constants';
import { StoreContext } from '../../../contexts';
import { v4 as uuidv4 } from 'uuid';
import { ownerServices } from '../../../services';
import {
  BellOutlined,
  CameraOutlined,
  DashboardOutlined,
  DollarOutlined,
  HomeOutlined,
  InfoCircleOutlined,
} from '@ant-design/icons';
import NearByLocationTag from '../AccommodationCreate/NearByLocationTag';
import UploadImage from '../AccommodationCreate/UploadImage';

const Option = Select.Option;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

const buttonItemLayout = {
  wrapperCol: { span: 14, offset: 6 },
};

const validateMessages = {
  required: '${label} bắt buộc nhập!',
  types: {
    email: '${label} không là email khả dụng!',
    number: '${label} không là một số khả dụng!',
  },
  number: {
    range: '${label} phải nằm trong khoảng ${min} và ${max}',
  },
};

const { BATHROOM_TYPES, KITCHEN_TYPES } = accommodationConstants;

const RoomEdit = (props: IProps) => {
  const { room, isLoading } = accommodationHooks.useRoomEdit(props.roomId);

  const [form] = Form.useForm();
  const [nearbyLocations, setNearbyLocations] = useState<string[]>([]);

  const { cities, districts, wards, setCityCode, setDistrictCode } = addressHooks.useAddressHook();
  const { accommodationTypes } = accommodationHooks.useTypes();
  const { fileList, setFileList, getImages } = fileHooks.useImage();

  const { currentUser } = useContext(StoreContext);

  const [numberOfDisplay, setNumberOfDisplay] = useState(0);

  useEffect(() => {
    if (room) {
      setNearbyLocations(room.nearby_locations);
      setNumberOfDisplay(room.number_of_days_display);
      setCityCode(room.city_code);
      setDistrictCode(room.district_code);
      setFileList(room.images.map(image => {
        return {
          uid: uuidv4().toString(),
          url: image
        } as any
      }))
    }
  }, [room]);

  const onFinish = async (values: any) => {
    if (fileList.length < 3) {
      notification.error({
        message: 'Đảm bảo rằng bạn đã tải lên ít nhất 3 tấm ảnh',
        duration: 1,
      });
      return false;
    }
    console.log({
      ...values,
      nearby_locations: nearbyLocations,
      id: room?.id,
      owner_id: currentUser.id,
      images: getImages(),
    });
    ownerServices.updateAccommodation({
      ...values,
      nearby_locations: nearbyLocations,
      id: room?.id,
      owner_id: currentUser.id,
      images: getImages(),
    }).then(() => {
      notification.success({
        message: 'Cập nhật thông tin phòng trọ thành công.',
        duration: 3,
      });
      form.resetFields();
    });
  };

  if (!room && isLoading) {
    return (
      <div style={{ width: "100%"}} className={"text-center"}>
        <Spin className={'app-spinner m-base text-center'} />;
      </div>
    )
  }

  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      validateMessages={validateMessages}
      form={form}
      className={'form'}
    >
      <h3><InfoCircleOutlined /> Thông tin chung </h3>
      <Form.Item
        label="Tiêu đề"
        name={'title'}
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.title}
      >
        <Input placeholder="Homestay Ngã Tư Sở Cao Cấp - Văn Minh - Sạch Sẽ ..." />
      </Form.Item>

      <Form.Item
        name={'accommodation_type_id'}
        label="Loại phòng"
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.accommodation_type_id}
      >
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Chọn một loại phòng"
          optionFilterProp="children"
          filterOption={(input, option) =>
            (option as any).children
              .toLowerCase()
              .indexOf(input.toLowerCase()) >= 0
          }
        >
          {accommodationTypes &&
          accommodationTypes.map((type, idx) => (
            <Option value={type.id} key={idx}>
              {type.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name={'area'}
        label={<span>Diện tích (m <sup>2</sup>) </span>}
        messageVariables={{ label: 'Diện tích' }}
        labelAlign={'left'}
        rules={[{ required: true, type: 'number', min: 1, max: 2000 }]}
        initialValue={room?.area}
      >
        <InputNumber className={'input-number'} placeholder={'Ví dụ: 20'} />
      </Form.Item>

      <Form.Item
        name={'number_of_room'}
        label={'Số phòng'}
        labelAlign={'left'}
        rules={[{ required: true, type: 'number', min: 1, max: 30 }]}
        initialValue={room?.number_of_room}
      >
        <InputNumber className={'input-number'} placeholder={'Ví dụ: 1'} />
      </Form.Item>

      <Form.Item
        name={'is_stay_with_the_owner'}
        label={'Chung chủ'}
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.is_stay_with_the_owner}
      >
        <Radio.Group>
          <Radio value={true}>Có</Radio>
          <Radio value={false}>Không</Radio>
        </Radio.Group>
      </Form.Item>

      <h3><HomeOutlined /> Địa chỉ </h3>
      <Form.Item
        name={'city_code'}
        label="Tỉnh/Thành phố"
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.city_code}
      >
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Chọn một thành phố"
          optionFilterProp="children"
          onChange={(code: string) => {
            form.setFieldsValue({
              ['ward_code']: undefined,
            });
            setCityCode(code);
          }}
          filterOption={(input, option) =>
            (option as any).children
              .toLowerCase()
              .indexOf(input.toLowerCase()) >= 0
          }
        >
          {cities &&
          cities.map((city, idx) => (
            <Option value={city.code} key={idx}>
              {city.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name={'district_code'}
        label="Quận/Huyện"
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.district_code}
      >
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Chọn một quận/huyện"
          optionFilterProp="children"
          onChange={(code: string) => {
            form.setFieldsValue({
              ['ward_code']: undefined,
            });
            setDistrictCode(code);
          }}
          filterOption={(input, option) =>
            (option as any).children
              .toLowerCase()
              .indexOf(input.toLowerCase()) >= 0
          }
        >
          {districts &&
          districts.map((district, idx) => (
            <Option value={district.code} key={idx}>
              {district.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name={'ward_code'}
        label="Phường/Xã/Thị trấn"
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.ward_code}
      >
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Chọn một phường/xã/thị trấn"
          optionFilterProp="children"
          filterOption={(input, option) =>
            (option as any).children
              .toLowerCase()
              .indexOf(input.toLowerCase()) >= 0
          }
        >
          {wards &&
          wards.map((ward, idx) => (
            <Option value={ward.code} key={idx}>
              {ward.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        label="Địa chỉ đường/ngách"
        labelAlign={'left'}
        name={'street_address'}
        rules={[{ required: true }]}
        initialValue={room?.street_address}
      >
        <Input placeholder="59 Khúc Thừa Dụ" />
      </Form.Item>

      <Form.Item
        label="* Địa chỉ lân cận"
        labelAlign={'left'}
      >
        <NearByLocationTag setTags={setNearbyLocations} tags={nearbyLocations} />
      </Form.Item>

      <h3><DollarOutlined /> Giá cả</h3>
      <Form.Item
        name={'price'}
        label="Giá phòng (đồng/tháng)"
        messageVariables={{ label: 'Giá phòng' }}
        labelAlign={'left'}
        rules={[{ type: 'number', min: 100000, max: 100000000, required: true }]}
        initialValue={room?.price}
      >
        <InputNumber className={'input-number'} />
        {/*<span>Đồng</span>*/}
      </Form.Item>
      <Form.Item
        name={'electricity_price'}
        label="Giá điện (đồng/số)"
        messageVariables={{ label: 'Giá điện' }}
        labelAlign={'left'}
        rules={[{ type: 'number', min: 1000, max: 7000, required: true }]}
        initialValue={room?.electricity_price}
      >
        <InputNumber className={'input-number'} />
        {/*<span>Đồng</span>*/}
      </Form.Item>
      <Form.Item
        name={'water_price'}
        label="Giá nước (đồng/khối)"
        messageVariables={{ label: 'Giá nước' }}
        labelAlign={'left'}
        rules={[{ type: 'number', min: 1000, max: 7000, required: true }]}
        initialValue={room?.water_price}
      >
        <InputNumber className={'input-number'} />
        {/*<span>Đồng</span>*/}
      </Form.Item>

      <h3><BellOutlined /> Tiện ích</h3>
      <Form.Item
        name={'bathroom_type'}
        label="Nhà tắm"
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.bathroom_type}
      >
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Chọn loại nhà tắm"
          optionFilterProp="children"
          filterOption={(input, option) =>
            (option as any).children
              .toLowerCase()
              .indexOf(input.toLowerCase()) >= 0
          }
        >
          {BATHROOM_TYPES &&
          BATHROOM_TYPES.map((type, idx) => (
            <Option value={type.value} key={idx}>
              {type.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name={'kitchen_type'}
        label="Nhà bếp"
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.kitchen_type}
      >
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Chọn loại nhà bếp"
          optionFilterProp="children"
          filterOption={(input, option) =>
            (option as any).children
              .toLowerCase()
              .indexOf(input.toLowerCase()) >= 0
          }
        >
          {KITCHEN_TYPES &&
          KITCHEN_TYPES.map((type, idx) => (
            <Option value={type.value} key={idx}>
              {type.name}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name={'has_air_conditioning'}
        label={'Điều hòa'}
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.has_air_conditioning}
      >
        <Radio.Group>
          <Radio value={true}>Có</Radio>
          <Radio value={false}>Không</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name={'has_electric_water_heater'}
        label={'Bình nóng lạnh'}
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.has_electric_water_heater}
      >
        <Radio.Group>
          <Radio value={true}>Có</Radio>
          <Radio value={false}>Không</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name={'has_balcony'}
        label={'Ban công'}
        labelAlign={'left'}
        rules={[{ required: true }]}
        initialValue={room?.has_balcony}
      >
        <Radio.Group>
          <Radio value={true}>Có</Radio>
          <Radio value={false}>Không</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name={'description'}
        label="* Mô tả thêm ( tiện ích, ... )"
        labelAlign={'left'}
        initialValue={room?.description}
      >
        <Input.TextArea
          placeholder="Henry Homestay trọn gói 1,6 -1.7 triệu / Người / tháng. "
          rows={6}
          showCount
          maxLength={3000}
        />
      </Form.Item>

      <h3><CameraOutlined /> Ảnh</h3>
      <Form.Item
        label={'* Tải ảnh lên ( ít nhất 3 ảnh )'}
        labelAlign={'left'}
      >
        <UploadImage fileList={fileList} setFileList={setFileList} />
      </Form.Item>

      <h3><DashboardOutlined /> Lên lịch hiển thị</h3>

      <Form.Item
        name={'number_of_days_display'}
        label="Số ngày hiển thị trên web (ngày)"
        messageVariables={{ label: 'Ngày hiển thị' }}
        labelAlign={'left'}
        rules={[{ type: 'number', min: 1, max: 60, required: true }]}
        initialValue={room?.number_of_days_display}
      >
        <InputNumber
          className={'input-number'}
          placeholder={'Ví dụ : 7'}
          onChange={(value) => {
            if (value && typeof value == 'number') {
              setNumberOfDisplay(value);
            } else {
              setNumberOfDisplay(0);
            }
          }}
        />
      </Form.Item>

      {numberOfDisplay ? (
        <Form.Item
          label={'* Số tiền cần trả'}
          labelAlign={'left'}
        >
          <b>{numberOfDisplay * 1000} đồng</b>
        </Form.Item>
      ) : null}

      <Form.Item {...buttonItemLayout}>
        <Button type="primary" htmlType="submit"> Cập nhật ngay </Button>
      </Form.Item>
    </Form>
  );
};

interface IProps {
  roomId: string;
}

export default RoomEdit;