import React, { useContext, useEffect, useState } from 'react';
import { Button, Form, Input, InputNumber, Modal, notification, Radio, Select } from 'antd';
import { accommodationHooks, addressHooks, fileHooks } from '../../../hooks';
import {
  EditOutlined,
  InfoCircleOutlined,
  HomeOutlined,
  BellOutlined,
  DollarOutlined,
  CameraOutlined,
  DashboardOutlined,
} from '@ant-design/icons';
import NearByLocationTag from './NearByLocationTag';
import { v4 as uuidv4 } from 'uuid';

import './AccommodationCreate.scss';
import { accommodationConstants } from '../../../constants';
import { StoreContext } from '../../../contexts';
import UploadImage from './UploadImage';
import { ownerServices } from '../../../services';

const Option = Select.Option;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

const buttonItemLayout = {
  wrapperCol: { span: 14, offset: 6 },
};

const validateMessages = {
  required: '${label} bắt buộc nhập!',
  types: {
    email: '${label} không là email khả dụng!',
    number: '${label} không là một số khả dụng!',
  },
  number: {
    range: '${label} phải nằm trong khoảng ${min} và ${max}',
  },
};

const { BATHROOM_TYPES, KITCHEN_TYPES } = accommodationConstants;


const AccommodationCreate = (props: IProps) => {
  const [form] = Form.useForm();
  const [nearbyLocations, setNearbyLocations] = useState<string[]>(['Đại học Công nghệ']);

  const { cities, districts, wards, setCityCode, setDistrictCode } = addressHooks.useAddressHook();
  const { accommodationTypes } = accommodationHooks.useTypes();

  const { currentUser } = useContext(StoreContext);

  const { fileList, setFileList, getImages } = fileHooks.useImage();

  const [numberOfDisplay, setNumberOfDisplay] = useState(0);

  const onFinish = async (values: any) => {
    if (fileList.length < 3) {
      notification.error({
        message: 'Đảm bảo rằng bạn đã tải lên ít nhất 3 tấm ảnh',
        duration: 1,
      });
      return false;
    }
    console.log({
      ...values,
      nearby_locations: nearbyLocations,
      id: uuidv4().toString(),
      owner_id: currentUser.id,
      images: getImages(),
    });
    ownerServices.createAccommodation({
      ...values,
      nearby_locations: nearbyLocations,
      id: uuidv4().toString(),
      owner_id: currentUser.id,
      images: getImages(),
    }).then(() => {
      notification.success({
        message: 'Đăng phòng trọ thành công. Bài viết đang được phê duyệt.',
        duration: 3,
      });
      form.resetFields();
      props.handleCancel();
      props.handleRefresh(true);
    });
  };

  // @ts-ignore
  return (
    <Modal
      title={<span>Đăng một phòng mới <EditOutlined /> </span>}
      visible={props.isVisible}
      onOk={props.handleOk}
      onCancel={props.handleCancel}
      width={1000}
      style={{ top: 20, height: 100 }}
      className={'modal-form'}
      footer={null}
    >
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        validateMessages={validateMessages}
        form={form}
        className={'form'}
      >
        <h3><InfoCircleOutlined /> Thông tin chung </h3>
        <Form.Item
          label="Tiêu đề"
          name={'title'}
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Input placeholder="Homestay Ngã Tư Sở Cao Cấp - Văn Minh - Sạch Sẽ ..." />
        </Form.Item>

        <Form.Item
          name={'accommodation_type_id'}
          label="Loại phòng"
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Chọn một loại phòng"
            optionFilterProp="children"
            filterOption={(input, option) =>
              (option as any).children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {accommodationTypes &&
            accommodationTypes.map((type, idx) => (
              <Option value={type.id} key={idx}>
                {type.name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name={'area'}
          label={<span>Diện tích (m <sup>2</sup>) </span>}
          messageVariables={{ label: 'Diện tích' }}
          labelAlign={'left'}
          rules={[{ required: true, type: 'number', min: 1, max: 2000 }]}
        >
          <InputNumber className={'input-number'} placeholder={'Ví dụ: 20'} />
        </Form.Item>

        <Form.Item
          name={'number_of_room'}
          label={'Số phòng'}
          labelAlign={'left'}
          rules={[{ required: true, type: 'number', min: 1, max: 30 }]}
        >
          <InputNumber className={'input-number'} placeholder={'Ví dụ: 1'} />
        </Form.Item>

        <Form.Item
          name={'is_stay_with_the_owner'}
          label={'Chung chủ'}
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Radio.Group>
            <Radio value={true}>Có</Radio>
            <Radio value={false}>Không</Radio>
          </Radio.Group>
        </Form.Item>

        <h3><HomeOutlined /> Địa chỉ </h3>
        <Form.Item
          name={'city_code'}
          label="Tỉnh/Thành phố"
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Chọn một thành phố"
            optionFilterProp="children"
            onChange={(code: string) => {
              form.setFieldsValue({
                ['ward_code']: undefined,
              });
              setCityCode(code);
            }}
            filterOption={(input, option) =>
              (option as any).children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {cities &&
            cities.map((city, idx) => (
              <Option value={city.code} key={idx}>
                {city.name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name={'district_code'}
          label="Quận/Huyện"
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Chọn một quận/huyện"
            optionFilterProp="children"
            onChange={(code: string) => {
              form.setFieldsValue({
                ['ward_code']: undefined,
              });
              setDistrictCode(code);
            }}
            filterOption={(input, option) =>
              (option as any).children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {districts &&
            districts.map((district, idx) => (
              <Option value={district.code} key={idx}>
                {district.name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name={'ward_code'}
          label="Phường/Xã/Thị trấn"
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Chọn một phường/xã/thị trấn"
            optionFilterProp="children"
            filterOption={(input, option) =>
              (option as any).children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {wards &&
            wards.map((ward, idx) => (
              <Option value={ward.code} key={idx}>
                {ward.name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label="Địa chỉ đường/ngách"
          labelAlign={'left'}
          name={'street_address'}
          rules={[{ required: true }]}
        >
          <Input placeholder="59 Khúc Thừa Dụ" />
        </Form.Item>

        <Form.Item
          label="* Địa chỉ lân cận"
          labelAlign={'left'}
        >
          <NearByLocationTag setTags={setNearbyLocations} tags={nearbyLocations} />
        </Form.Item>

        <h3><DollarOutlined /> Giá cả</h3>
        <Form.Item
          name={'price'}
          label="Giá phòng (đồng/tháng)"
          messageVariables={{ label: 'Giá phòng' }}
          labelAlign={'left'}
          rules={[{ type: 'number', min: 100000, max: 100000000, required: true }]}
        >
          <InputNumber className={'input-number'} />
          {/*<span>Đồng</span>*/}
        </Form.Item>
        <Form.Item
          name={'electricity_price'}
          label="Giá điện (đồng/số)"
          messageVariables={{ label: 'Giá điện' }}
          labelAlign={'left'}
          rules={[{ type: 'number', min: 1000, max: 7000, required: true }]}
        >
          <InputNumber className={'input-number'} />
          {/*<span>Đồng</span>*/}
        </Form.Item>
        <Form.Item
          name={'water_price'}
          label="Giá nước (đồng/khối)"
          messageVariables={{ label: 'Giá nước' }}
          labelAlign={'left'}
          rules={[{ type: 'number', min: 1000, max: 7000, required: true }]}
        >
          <InputNumber className={'input-number'} />
          {/*<span>Đồng</span>*/}
        </Form.Item>

        <h3><BellOutlined /> Tiện ích</h3>
        <Form.Item
          name={'bathroom_type'}
          label="Nhà tắm"
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Chọn loại nhà tắm"
            optionFilterProp="children"
            filterOption={(input, option) =>
              (option as any).children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {BATHROOM_TYPES &&
            BATHROOM_TYPES.map((type, idx) => (
              <Option value={type.value} key={idx}>
                {type.name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name={'kitchen_type'}
          label="Nhà bếp"
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Chọn loại nhà bếp"
            optionFilterProp="children"
            filterOption={(input, option) =>
              (option as any).children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            {KITCHEN_TYPES &&
            KITCHEN_TYPES.map((type, idx) => (
              <Option value={type.value} key={idx}>
                {type.name}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name={'has_air_conditioning'}
          label={'Điều hòa'}
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Radio.Group>
            <Radio value={true}>Có</Radio>
            <Radio value={false}>Không</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item
          name={'has_electric_water_heater'}
          label={'Bình nóng lạnh'}
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Radio.Group>
            <Radio value={true}>Có</Radio>
            <Radio value={false}>Không</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item
          name={'has_balcony'}
          label={'Ban công'}
          labelAlign={'left'}
          rules={[{ required: true }]}
        >
          <Radio.Group>
            <Radio value={true}>Có</Radio>
            <Radio value={false}>Không</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item
          name={'description'}
          label="* Mô tả thêm ( tiện ích, ... )"
          labelAlign={'left'}
        >
          <Input.TextArea
            placeholder="Henry Homestay trọn gói 1,6 -1.7 triệu / Người / tháng. "
            rows={6}
            showCount
            maxLength={3000}
          />
        </Form.Item>

        <h3><CameraOutlined /> Ảnh</h3>
        <Form.Item
          label={'* Tải ảnh lên ( ít nhất 3 ảnh )'}
          labelAlign={'left'}
        >
          <UploadImage fileList={fileList} setFileList={setFileList} />
        </Form.Item>

        <h3><DashboardOutlined /> Lên lịch hiển thị</h3>

        <Form.Item
          name={'number_of_days_display'}
          label="Số ngày hiển thị trên web (ngày)"
          messageVariables={{ label: 'Ngày hiển thị' }}
          labelAlign={'left'}
          rules={[{ type: 'number', min: 1, max: 60, required: true }]}
        >
          <InputNumber
            className={'input-number'}
            placeholder={'Ví dụ : 7'}
            onChange={(value) => {
              if (value && typeof value == 'number') {
                setNumberOfDisplay(value);
              } else {
                setNumberOfDisplay(0);
              }
            }}
          />
        </Form.Item>

        {numberOfDisplay ? (
          <Form.Item
            label={'* Số tiền cần trả'}
            labelAlign={'left'}
          >
            <b>{numberOfDisplay * 1000} đồng</b>
          </Form.Item>
        ) : null}

        <Form.Item {...buttonItemLayout}>
          <Button type="primary" htmlType="submit"> Đăng ngay </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

interface IProps {
  isVisible: boolean;
  handleOk: () => any;
  handleCancel: () => any;
  handleRefresh: any;
}

export default AccommodationCreate;