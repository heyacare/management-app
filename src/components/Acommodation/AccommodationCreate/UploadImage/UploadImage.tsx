import React, { useState } from 'react';
import { Modal, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { fileServices } from '../../../../services';
import { RcFile } from 'antd/es/upload';

const getBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
};

const UploadImage = (props: Iprops) => {

  const [loading] = useState(false);
  const { fileList, setFileList } = props;

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [previewTitle, setPreviewTitle] = useState('');

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewVisible(true);
    setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
  };

  // @ts-ignore
  const handleChange = ({ fileList }) => setFileList(fileList);

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const uploadFile = (
    onSuccess: (response: object, file: RcFile) => void,
    onError: (error: Error, response?: any, file?: (RcFile | undefined)) => void,
    file: RcFile,
  ) => {
    let formData = new FormData();
    formData.append('file', file);
    fileServices.uploadImage(formData)
      .then((res: any) => {
        onSuccess(res.data["images-url"][0], file);
      }).catch(error => onError(error, null, undefined));
  };


  return (
    <>
      <Upload
        multiple={true}
        showUploadList={true}
        name="file"
        listType="picture-card"
        fileList={fileList}
        customRequest={({ onSuccess, onError, file }) => uploadFile(onSuccess, onError, file)}
        onPreview={handlePreview}
        onChange={handleChange}
      >
        {fileList.length >= 8 ? null : uploadButton}
      </Upload>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
        width={800}
      >
        <img alt="example" style={{ width: '100%' }} src={previewImage} />
      </Modal>
    </>
  );
};

interface Iprops {
  fileList: any;
  setFileList: any
}

export default UploadImage;