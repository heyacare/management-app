import React, { useRef, useState } from 'react';
import { Tag, Input } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import { PlusOutlined } from '@ant-design/icons';
import './NearByLocationTag.scss';

const colors = ['green', 'cyan', 'blue', 'geekblue', 'purple', 'volcano', 'red', 'magenta', 'orange'];

const randomColor = () => {
  return colors[Math.floor(Math.random() * colors.length)];
};

const NearByLocationTag = (props: IProps) => {
    const { tags, setTags } = props;

    const [inputVisible, setInputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>('');

    const handleClose = (removedTag: string) => {
      const newTags = tags.filter(tag => tag !== removedTag);
      setTags(newTags);
    };

    const showInput = () => {
      setInputVisible(true);
    };

    const handleInputChange = (e: any) => {
      setInputValue(e.target.value);
    };

    const handleInputConfirm = () => {
      let newTags: string[] = [];
      if (inputValue && tags.indexOf(inputValue) === -1) {
        newTags = [...tags, inputValue];
      }
      setTags(newTags);
      setInputVisible(false);
      setInputValue('');
    };


    const forMap = (tag: string) => {
      const tagElem = (
        <Tag
          className="tag"
          color={'purple'}
          closable
          onClose={e => {
            e.preventDefault();
            handleClose(tag);
          }}
        >
          {tag}
        </Tag>
      );
      return (
        <span key={tag} style={{ display: 'inline-block' }}>
        {tagElem}
      </span>
      );
    };

    const tagChild = tags.map(forMap);

    return (
      <div className={'nearby-location-tag'}>
        <div style={{ marginBottom: 16 }}>
          <TweenOneGroup
            enter={{
              scale: 0.8,
              opacity: 0,
              type: 'from',
              duration: 100,
            }}
            leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
            appear={false}
          >
            {tagChild}
          </TweenOneGroup>
        </div>
        {inputVisible && (
          <Input
            autoFocus
            type="text"
            size="small"
            style={{ width: 78 }}
            value={inputValue}
            onChange={handleInputChange}
            onBlur={handleInputConfirm}
            onPressEnter={handleInputConfirm}
            className={'tag-input'}
          />
        )}
        {!inputVisible && (
          <Tag onClick={showInput} className="tag site-tag-plus">
            <PlusOutlined /> Thêm địa chỉ mới
          </Tag>
        )}
      </div>
    );
  }
;

interface IProps {
  setTags: (tags: string[]) => void;
  tags: string[];
}

export default NearByLocationTag;