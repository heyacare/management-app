import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Spin } from 'antd';
import { AuthHooks } from 'hooks';
import { authServices } from 'services';
import { IRoute } from 'interfaces';
import { StoreContext } from 'contexts';

const PrivateRoute = ({ component: Component, ...rest }: IRoute) => {
  useEffect(() => {
  }, []);

  // Check if user is logged in or not
  if (!authServices.isLoggedIn()) {
    return (
      <Route
        {...rest}
        render={routeProps => (
          <Redirect
            {...routeProps}
            to={{ pathname: '/login', state: { from: routeProps.location } }}
          />
        )}
      />
    );
  }

  // Fetch global data
  const { currentUser } = AuthHooks.useUserInfo();

  // Show spin when fetching required global data
  if (!currentUser) {
    return <Spin className="app-spin" />;
  }

  return (
    <StoreContext.Provider
      value={{
        currentUser,
      }}
    >
      <Route {...rest} render={routeProps => <Component {...routeProps} />} />
    </StoreContext.Provider>
  );
};

export default PrivateRoute;
