import { lazy } from 'react';
import {
  HomeOutlined,
  AppstoreOutlined,
  UsergroupAddOutlined,
} from '@ant-design/icons';
import { roleConstants } from 'constants/index';
import { t } from 'helpers/i18n';
import ChatAmin from '../containers/ChatAmin/ChatAdmin';
import ApprovalAccommodation from '../containers/Post/ApprovalAccommodation';
import Analytics from '../containers/Analytics';
import ApprovalRating from '../containers/Post/ApprovalRating';
import Profile from '../containers/Profile';

const { ADMIN, OWNER } = roleConstants;

// App pages
const Home = lazy(() => import('containers/Home'));
const Accommodation = lazy(() => import('containers/Post/Accommodation'));
const ApprovalOwner = lazy(() => import('containers/User/ApprovalOwner'));
/*
 * If route has children, it's a parent menu (not link to any pages)
 * You can change permissions to your permissions for testing
 */
const routes = [
  // This is a menu/route which has no children (sub-menu)
  {
    exact: true,
    path: '/',
    name: t('Trang chủ'),
    component: Home,
    icon: HomeOutlined,
  },
  // This is a parent menu which has children (sub-menu) and requires catalog:product:X permission to display
  // X maybe read/create/update/delete...
  {
    path: '/managements/accommodations',
    name: t('Quản lý phòng trọ'),
    icon: AppstoreOutlined,
    children: [
      '/managements/posts',
      '/managements/posts/approve',
      '/managements/posts/analytics',
      '/managements/ratings/approve',
    ], // Specifies sub-menus/routes (sub-menu path)
    roles: [OWNER, ADMIN],
  },
  // This is a sub-menu/route which requires permission to display/access
  {
    exact: true,
    path: '/managements/posts/analytics',
    name: t('Thống kê'),
    component: Analytics,
    roles: [ADMIN],
  },
  {
    exact: true,
    path: '/managements/posts',
    name: t('Bài đăng cá nhân'),
    component: Accommodation,
    roles: [OWNER, ADMIN],
  },
  {
    exact: true,
    path: '/managements/posts/approve',
    name: t('Phê duyệt bài đăng'),
    component: ApprovalAccommodation,
    roles: [ADMIN],
  },
  {
    exact: true,
    path: '/managements/ratings/approve',
    name: t('Phê duyệt đánh giá'),
    component: ApprovalRating,
    roles: [ADMIN],
  },
  {
    path: '/managements/users',
    name: t('Quản lý chủ trọ'),
    icon: UsergroupAddOutlined,
    children: [
      '/managements/users/list',
      '/managements/users/approve',
      '/profile',
    ], // Specifies sub-menus/routes (sub-menu path)
    roles: [ADMIN],
  },
  {
    path: '/managements/users/list',
    name: t('Danh sách chủ trọ'),
    component: Home,
    roles: [ADMIN],
  },
  {
    path: '/managements/users/approve',
    name: t('Phê duyệt chủ trọ'),
    component: ApprovalOwner,
    roles: [ADMIN],
  },
  {
    path: '/profile',
    name: t('Trang cá nhân'),
    component: Profile,
    roles: [ADMIN, OWNER],
  },
  {
    path: '/support/chat/:user_id',
    name: t('Hỗ trợ qua tin nhắn'),
    roles: [ADMIN],
    component: ChatAmin,
  },
];

export default routes;
