export interface IAccommodationOwner {
  id: string;
  title: string;
  full_address: string;
  number_of_views: number;
  average_rating: number;
  status: string;
  is_rented: boolean;
  images: string[];
  display_time: string;
}

export interface IAccommodationOwnerRes {
  total: number;
  accommodations: IAccommodationOwner[];
}

interface RoomType {
  id: number;
  name: string;
}

export interface IRoomPreview {
  id: string;
  owner: any;
  title: string;
  description: string;
  street_address: string;
  address: string;
  accommodation_type: string;
  number_of_room: number;
  price: number;
  area: number;
  electricity_price: number,
  water_price: number;
  is_stay_with_the_owner: boolean,
  is_rented: boolean,
  bathroom_type: string;
  kitchen_type: string;
  has_electric_water_heater: boolean;
  has_balcony: boolean;
  has_air_conditioning: boolean;
  created_at: string;
  images: string[];
  nearby_locations: string[];
}