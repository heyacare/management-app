import { Locale } from 'antd/lib/locale-provider';

export interface IRoute {
  exact?: boolean;
  path: string;
  name: string;
  component: React.ReactType;
  roles?: string[];
  icon?: React.ComponentType<{ className?: string }>;
  children?: string[];
}

export interface IRegionItem {
  key: string;
  name: string;
  flag: string;
  antdLocale: Locale;
}

export interface IRegion {
  [key: string]: IRegionItem;
}
